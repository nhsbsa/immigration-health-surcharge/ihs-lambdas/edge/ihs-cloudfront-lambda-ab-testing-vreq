import json
import random
import boto3

def initCookies(headers, cookieText):
    cookieTextA = '; ' + cookieText
    cookieTextB = cookieText + '; '
    parsedCookies = {}
    removeIndices = []
    index = 0
    rIndex = 0
    for bigCookies in headers.get('cookie', []):
        existingCookie = bigCookies['value']
        newCookie1 = existingCookie.replace(cookieTextA, '')
        newCookie2 = newCookie1.replace(cookieTextB, '')
        newCookie3 = newCookie2.replace(cookieText, '')
        if not newCookie3.strip():
            removeIndices.append(index)
        else:
            bigCookies['value'] = newCookie3.strip()
            parsedCookies[index] = newCookie3.strip()
        index = index + 1

    for index in reversed(removeIndices):
        del(headers['cookie'][index])
        
def parseCookies(headers):
    parsedCookies = {}
    index = 0
    for bigCookies in headers.get('cookie', []):
        for cookie in bigCookies['value'].split(';'):
            if cookie:
                parsedCookies[index] = cookie.strip()
                index = index + 1
    return parsedCookies

def getBWeighting():
    try:
        ssm          = boto3.client('ssm')
        ssmParameter = ssm.get_parameter(Name='/ab-testing/b-weighting')
        bWeighting   = float(ssmParameter['Parameter']['Value'])
        return bWeighting
    except:
        print("Error occured whilst retrieving B weighting from the parameter store - using default value!")
        return 0.50
    
def lambda_handler(event, context):
    request = event['Records'][0]['cf']['request']
    headers = request['headers']
    uri     = request['uri']
    
    landingPage       = '/health/eligibility'
    testDeciderPage   = '/health/claim/name'
    ihsDonePage       = '/health/claim/done'
    ihsPage           = '/health/'
    referrer          = ''
    isGetMethod       = False

    cookieExperiments = 'X-IHS-Experiment=true'
    cookieExperimentA = 'X-IHS-Experiment-Name=A'
    cookieExperimentB = 'X-IHS-Experiment-Name=B'
    createCookies     = False
    checkCookies      = False

    queryTestingA     = 'ihs-ab-testing=A'
    queryTestingB     = 'ihs-ab-testing=B'
    createQuery       = False
    useATesting       = False
    useBTesting       = False
    abTesting         = 'A'

    print('VIEWER-REQUEST:' + request['method'] + ' ' + uri)
    if 'cookie' not in request['headers']:
        request['headers']['cookie'] = []

    if 'referer' in request['headers']:
        referrer = request['headers']['referer'][0]['value']

    if 'method' in request:
        if request['method'] == 'GET':
            isGetMethod = True

    if landingPage in uri:
        print('Deleting A/B Testing Cookies')
        initCookies(headers, cookieExperimentA)
        initCookies(headers, cookieExperimentB)
    elif (uri == testDeciderPage) and isGetMethod:
        print('Deleting A/B Testing Cookies')
        initCookies(headers, cookieExperimentA)
        initCookies(headers, cookieExperimentB)
        createCookies = True
        createQuery   = True
    elif ihsDonePage in uri:
        checkCookies = False
        createQuery  = False
    elif ihsPage in uri:
        checkCookies = True
        createQuery  = True
    
    if checkCookies:   
        print('Checking for A/B Testing Cookies')
        parsedCookies = parseCookies(headers)
        if parsedCookies:
            for index, cookie in parsedCookies.items():
                if cookie == cookieExperimentA:
                    abTesting = queryTestingA
                    useATesting = True
                    print('Found A/B Testing Cookie for A')
                if cookie == cookieExperimentB:
                    abTesting = queryTestingB
                    useBTesting = True
                    print('Found A/B Testing Cookie for B')
            if useATesting and useBTesting:
                print('A/B Testing Cookies found for both A and B - deleting B Testing Cookies')
                useBTesting = False
                abTesting   = queryTestingA
                initCookies(headers, cookieExperimentB)
    
    if createCookies:
        weighting = random.random()
        abTestingWeight = getBWeighting()
        print("random weighting = " + str(weighting))
        print("configured B weighting = " + str(abTestingWeight))
        request['headers']['cookie'].append({'key': 'Cookie', 'value': cookieExperiments})
        if weighting > abTestingWeight:
            abTesting = queryTestingA
            useATesting = True
            useBTesting = False
            request['headers']['cookie'].append({'key': 'Cookie', 'value': cookieExperimentA})
            print('Creating A/B Testing Cookie for A')
        else:
            abTesting = queryTestingB
            useATesting = False
            useBTesting = True
            request['headers']['cookie'].append({'key': 'Cookie', 'value': cookieExperimentB})
            print('Creating A/B Testing Cookie for B')

    if createQuery:
        if useBTesting:
            request['querystring'] = queryTestingB
            print('Creating A/B Testing Query for B')
        else:
            print('Creating A/B Testing Query for A')
    
    print(request)
    return request
